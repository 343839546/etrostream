/* eslint global-require: 0, flowtype-errors/show-errors: 0 */

/**
 * This module executes inside of electron's main process. You can start
 * electron renderer process from here and communicate with the other processes
 * through IPC.
 *
 * When running `npm run build` or `npm run build-main`, this file is compiled to
 * `./app/main.prod.js` using webpack. This gives us some performance wins.
 *
 * @flow
 */
import { app, BrowserWindow, ipcMain as ipc } from 'electron';
import settings from 'electron-settings';
import peerflix from 'peerflix';
import address from 'network-address';
import proc from 'child_process';
import MenuBuilder from './menu';
import Spider from './spider/index';

let mainWindow = null;
let explorerWindow = null;
let peerflixEngine = null;
let vlc = null;

// settings default value
let isProxy = false;
let proxyLink = '';
let savedSites = [];

if (process.env.NODE_ENV === 'production') {
  const sourceMapSupport = require('source-map-support');
  sourceMapSupport.install();
}

if (process.env.NODE_ENV === 'development' || process.env.DEBUG_PROD === 'true') {
  require('electron-debug')();
  const path = require('path');
  const p = path.join(__dirname, '..', 'app', 'node_modules');
  require('module').globalPaths.push(p);
}

const installExtensions = async () => {
  const installer = require('electron-devtools-installer');
  const forceDownload = !!process.env.UPGRADE_EXTENSIONS;
  const extensions = [
    'REACT_DEVELOPER_TOOLS',
    'REDUX_DEVTOOLS'
  ];

  return Promise
    .all(extensions.map(name => installer.default(installer[name], forceDownload)))
    .catch(console.log);
};


/**
 * Add event listeners...
 */

app.on('window-all-closed', () => {
  // Respect the OSX convention of having the application in memory even
  // after all windows have been closed
  if (process.platform !== 'darwin') {
    app.quit();
  }
});


app.on('ready', async () => {
  if (process.env.NODE_ENV === 'development' || process.env.DEBUG_PROD === 'true') {
    await installExtensions();
  }

  isProxy = settings.get('isProxy');
  proxyLink = settings.get('proxyLink');
  savedSites = settings.get('savedSites');

  if (proxyLink === undefined || proxyLink === null || proxyLink === '') {
    proxyLink = 'socks5://127.0.0.1:2333';
  }


  mainWindow = new BrowserWindow({
    show: false,
    width: 1024,
    height: 728,
    resizable: false,
  });

  // 添加Chrome插件,需要toolbar支持,兼容性很差;v1.8新功能
  // BrowserWindow.addExtension(path)
  // await BrowserWindow.addExtension('');
  // /Users/liuyang/Downloads/SwitchyOmega-2.5.10/omega-target-chromium-extension/build

  // 子窗口
  // const child = new BrowserWindow({ parent: presWindow, modal: true, show: false });
  explorerWindow = new BrowserWindow({
    parent: mainWindow,
    show: false,
    webPreferences: {
      // devTools: true,
      // nodeIntegrationInWorker: true,
      // nodeIntegration: true,
      nodeIntegration: false,
      // preload: 'preload.js'
    }
  });

  // explorerWindow.loadURL('https://github.com');
  // explorerWindow.once('ready-to-show', () => {
  //   explorerWindow.show();
  //   mainWindow.webContents.send('explorer-loaded');
  // });
  explorerWindow.on('close', (e) => {
    if (explorerWindow.forceClose) return;
    e.preventDefault();
    explorerWindow.hide();
    explorerWindow.webContents.stop();
    mainWindow.webContents.send('explorer-hide');
  });

  explorerWindow.webContents.on('did-finish-load', () => {
    if (!mainWindow) {
      throw new Error('"mainWindow" is not defined');
    }
    // explorerWindow.webContents.executeJavaScript(`
    //   if (typeof module === 'object') {window.jQuery = window.$ = module.exports;};
    // `);

    explorerWindow.show();
    explorerWindow.openDevTools();
    mainWindow.webContents.send('explorer-loaded');
  });

  mainWindow.loadURL(`file://${__dirname}/app.html`);
  // @TODO: Use 'ready-to-show' event
  //        https://github.com/electron/electron/blob/master/docs/api/browser-window.md#using-ready-to-show-event
  mainWindow.webContents.on('did-finish-load', () => {
    if (!mainWindow) {
      throw new Error('"mainWindow" is not defined');
    }
    mainWindow.show();
    mainWindow.focus();
    // vWeb.setBounds({
    //   x: (1024 - 300), y: (728 - 300), width: 300, height: 300
    // });
    // vWeb.setAutoResize({ width: true, height: true });
    // vWeb.webContents.loadURL('https://electronjs.org');

    mainWindow.webContents.send('settings-receive', {
      isProxy,
      proxyLink,
      savedSites
    });
  });

  mainWindow.on('closed', () => {
    mainWindow = null;

    process.exit(0);
  });

  const menuBuilder = new MenuBuilder(mainWindow);
  menuBuilder.buildMenu();


  // vWeb = new BrowserView({
  //   webPreferences: {
  //     nodeIntegration: false
  //   }
  // });

  // mainWindow.setBrowserView(vWeb);
  // mainWindow.setBrowserView(vGUI);


  // ipc -----------------
  // ipc messages
  ipc.on('spy-magnet', () => {
    Spider.spy((dsMagnets) => {
      mainWindow.webContents.send('spy-magnet-res', dsMagnets);
      // playTorrent(dsMagnets[0].magnet);
    });
  });

  ipc.on('magnet-play', (eventEmitter, link) => {
    // console.log(link);
    playTorrent(link);
  });

  ipc.on('url-explorer', (eventEmitter, link) => {
    if (explorerWindow) {
      if (isProxy) {
        explorerWindow.webContents.session.setProxy({ proxyRules: proxyLink }, () => {
          explorerWindow.loadURL(link);
        });
      } else {
        explorerWindow.loadURL(link);
      }
    }
  });

  ipc.on('url-analyse-web-page', (eventEmitter) => {
    if (explorerWindow) {
      // console.log('url-analyse-web-page');
      explorerWindow.webContents.executeJavaScript(`
        (new Promise((resolve, reject) => { resolve(document.body.innerHTML); })).then(val => val);
      `, true)
        .then(innerHTML => {
          // console.log(innerHTML);
          Spider.regxFoundMagnet(innerHTML, (dsMagnets) => {
            mainWindow.webContents.send('spy-magnet-res', dsMagnets);
          });
          return innerHTML;
        }).catch(reason => { console.log(reason); });
    }
  });

  // .finally(innerHTML => {
  //   console.log('innerHTML');
  //   // Spider.regxFoundMagnet(innerHTML, (dsMagnets) => {
  //   //   mainWindow.webContents.send('spy-magnet-res', dsMagnets);
  //   // });
  // })
  // .catch(reason => { console.log(reason); });

  // ipc.on('explorer-window-inner-html', (_, innerHTML) => {
  //   // console.log(innerHTML);
  //   Spider.regxFoundMagnet(innerHTML, (dsMagnets) => {
  //     mainWindow.webContents.send('spy-magnet-res', dsMagnets);
  //   });
  // });

  // explorerWindow.webContents.executeJavaScript(`
  //   require('electron').ipcRenderer.send('explorer-window-inner-html', document.body.innerHTML);
  // `);

  // 设置功能部分
  ipc.on('settings-open-proxy', (eventEmitter, toOpen, link) => {
    console.log(`settings-open-proxy ${toOpen} ${link}`);
    isProxy = toOpen;
    proxyLink = link;

    settings.set('isProxy', isProxy);
    settings.set('proxyLink', proxyLink == null ? '' : proxyLink);
  });


  // default run;
  Spider.spy((dsMagnets) => {
    // console.log(dsMagnets);
    mainWindow.webContents.send('spy-magnet-res', dsMagnets);
    // playTorrent(dsMagnets[0].magnet);
  });
});

const playTorrent = (link) => {
  peerflixEngine = peerflix(link);
  peerflixEngine.server.on('listening', () => {
    const host = address();
    const href = `http://${host}:${peerflixEngine.server.address().port}/`;
    const localHref = `http://localhost:${peerflixEngine.server.address().port}/`;
    const filename = peerflixEngine.server.index.name.split('/').pop().replace(/\{|\}/g, '');
    const filelength = peerflixEngine.server.index.length;
    const paused = false;
    const timePaused = 0;
    const pausedAt = null;

    const VLC_ARGS = `-q${true ? ' --video-on-top' : ''} --play-and-exit`;
    // VLC_ARGS += ' --meta-title="' + filename.replace(/"/g, '\\"') + '"';

    // if (argv.all) {
    //   filename = engine.torrent.name
    //   filelength = engine.torrent.length
    //   href += '.m3u'
    //   localHref += '.m3u'
    // }

    const player = 'vlc';
    const root = '/Applications/VLC.app/Contents/MacOS/VLC';
    const home = (process.env.HOME || '') + root;
    vlc = proc.exec(
      `vlc ${VLC_ARGS} ${localHref} || ${root} ${VLC_ARGS} ${localHref} || ${home} ${VLC_ARGS} ${localHref}`,
      (error, stdout, stderror) => { if (error) { process.exit(0); } }
    );

    vlc.on('exit', () => {
      peerflixEngine.destroy(() => { console.log(peerflixEngine); });
    });
  });
};
