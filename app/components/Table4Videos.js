import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Table, {
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TableFooter,
  TablePagination, } from 'material-ui/Table';
import Paper from 'material-ui/Paper';
import IconButton from 'material-ui/IconButton';
import { PlayArrow } from 'material-ui-icons';
// import TablePaginationActions from './TablePaginationActions';  // 有bug, 不懂withStyles(styles) 的用法

// Table
const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 1,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
  button: {
    // margin: theme.spacing.unit,
  },
});

let counter = 0;
function createData(name, calories, fat) {
  counter += 1;
  return {
    id: counter, name, calories, fat
  };
}

type Props = {
  classes: {
    root: string,
    table: string
  },
  dataset: array,
  play: func
};

type State = {
  page: number,
  rowsPerPage: number
};

export default withStyles(styles)(class SimpleTable extends Component<Props, State> {
  props: Props;
  state: State;
  constructor(props, context) {
    super(props, context);
    this.state = {
      // dataset: [],
      // data: [].sort((a, b) => (a.calories < b.calories ? -1 : 1)),
      page: 0,
      rowsPerPage: 25,
    };
  }

  // componentWillReceiveProps(nextProps) {
  //   console.log(nextProps);
  //   this.setState({ dataset: nextProps.dataset });
  // }

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  handlePlayMagnetLink = (event, link) => {
    this.props.play(link);
  };

  render() {
    const { dataset, classes, play } = this.props;
    const { rowsPerPage, page } = this.state;
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, dataset.length - (page * rowsPerPage));
    // console.log('-----');
    // console.log(dataset);
    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell>Dessert (100g serving)</TableCell>
              <TableCell numeric>Calories</TableCell>
              <TableCell numeric>Fat (g)</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {dataset
            .slice(page * rowsPerPage, (page * rowsPerPage) + rowsPerPage)
            .map((n, i4ds) => (
              <TableRow key={`magnetid${i4ds}`}>
                <TableCell>{n.title}</TableCell>
                <TableCell numeric>n.magnet</TableCell>
                <TableCell numeric>
                  <IconButton
                    color="primary"
                    size="small"
                    className={classes.button}
                    aria-label="播放"
                    onClick={event => { this.handlePlayMagnetLink(event, n.magnet); }}
                  >
                    <PlayArrow />
                  </IconButton>

                  {/* <IconButton
                      // onClick={event => { this.handlePlayMagnetLink(event, n.magnet); }}
                      // disabled={page === 0}
                      aria-label="播放"
                  >
                    <PlayArrow />
                  </IconButton> */}
                </TableCell>
              </TableRow>
              ))}
            {emptyRows > 0 && (
              <TableRow style={{ height: 48 * emptyRows }}>
                <TableCell colSpan={6} />
              </TableRow>
            )}
          </TableBody>
          <TableFooter>
            <TableRow>
              <TablePagination
                colSpan={3}
                count={dataset.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onChangePage={this.handleChangePage}
                onChangeRowsPerPage={this.handleChangeRowsPerPage}
                // Actions={TablePaginationActions}
              />
            </TableRow>
          </TableFooter>
        </Table>
      </Paper>
    );
  }
});
