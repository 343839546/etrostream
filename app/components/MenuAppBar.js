// @flow
import { ipcRenderer as ipc } from 'electron';
import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui-icons/Menu';
import AccountCircle from 'material-ui-icons/AccountCircle';
import Switch from 'material-ui/Switch';
import { FormControlLabel, FormGroup } from 'material-ui/Form';
import Menu, { MenuItem } from 'material-ui/Menu';
import AppSearch from '../components/AppSearch';

const styles = {
  root: {
    flexGrow: 1,
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
  },
  flex: {
    flex: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
};

type Props = {
  classes: {
    root: string,
    flex: string,
    menuButton: string
  }
};

type State = {
  openProxy: boolean,
  auth: boolean,
  anchorEl: object,
  settings: object,
  proxyLink: string
};

class MenuAppBar extends React.Component<Props, State> {
  props: Props;
  state: State;

  constructor(props, context) {
    super(props, context);
    this.state = {
      openProxy: false,
      auth: true,
      anchorEl: null,
      proxyLink: ''
    };
  }

  componentWillMount() {
    ipc.on('settings-receive', this.settingsReceiver);
  }

  componentWillUnmount() {
    ipc.removeListener('settings-receive', this.settingsReceiver);
  }

  settingsReceiver = (eventEmitter, mainSettings) => {
    console.log(mainSettings);
    // {isProxy: true, proxyLink: "asdfasdfasdf"}
    this.setState({ openProxy: mainSettings.isProxy, proxyLink: mainSettings.proxyLink });
  };

  handleProxyChange = (event, checked) => {
    this.setState({ openProxy: checked });
    ipc.send('settings-open-proxy', checked, this.state.proxyLink);
  };

  handleSearchChanged = (event) => {
    this.setState({ proxyLink: event.target.value });
  };

  handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    const { classes } = this.props;
    const {
      auth, anchorEl,
      openProxy, proxyLink
    } = this.state;
    const open = Boolean(anchorEl);
    // if (settings === null) {
    //   openProxy = false;
    // } else {
    //   openProxy = Boolean(settings.proxy);
    // }

    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            <IconButton className={classes.menuButton} color="inherit" aria-label="Menu">
              <MenuIcon />
            </IconButton>
            <Typography variant="title" color="inherit" className={classes.flex}>
              Title
            </Typography>
            {!openProxy ? <AppSearch onChange={this.handleSearchChanged} history={proxyLink} /> : ''}
            {auth && (
              <div>
                <IconButton
                  aria-owns={open ? 'menu-appbar' : null}
                  aria-haspopup="true"
                  onClick={this.handleMenu}
                  color="inherit"
                >
                  <AccountCircle />
                </IconButton>
                <Menu
                  id="menu-appbar"
                  anchorEl={anchorEl}
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  open={open}
                  onClose={this.handleClose}
                >
                  <MenuItem onClick={this.handleClose}>Profile</MenuItem>
                  <MenuItem onClick={this.handleClose}>My account</MenuItem>
                </Menu>
              </div>
            )}
            <FormGroup>
              <FormControlLabel
                control={
                  <Switch checked={openProxy} onChange={this.handleProxyChange} aria-label="代理开关" />
                }
                label={openProxy ? '代理已开' : '代理已关'}
              />
            </FormGroup>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

MenuAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MenuAppBar);
