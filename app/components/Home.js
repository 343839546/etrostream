// @flow
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { ipcRenderer as ipc } from 'electron';
import Button from 'material-ui/Button';
import styles from './Home.css';


type Props = {};

export default class Home extends Component<Props> {
  props: Props;
  static handleClick() {
    ipc.send('spy-magnet');
  }

  static printtt(event, ds) {
    console.dir(ds);
  }

  componentWillMount() {
    ipc.on('spy-magnet-res', Home.printtt);
  }

  componentWillUnmount() {
    ipc.removeListener('spy-magnet-res', Home.printtt);
  }

  render() {
    return (
      <div>
        <div className={styles.container} data-tid="container">
          <Button variant="raised" color="primary" onClick={() => { Home.handleClick(); }}>
              Hello Wor33
          </Button>
          <h2>Home</h2>
          <Link to="/counter">to Counter</Link>
        </div>
      </div>
    );
  }
}
