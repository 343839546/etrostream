// @flow
import { ipcRenderer as ipc } from 'electron';
import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';
import TextField from 'material-ui/TextField';
import Snackbar from 'material-ui/Snackbar';
import Slide from 'material-ui/transitions/Slide';
import { InputAdornment } from 'material-ui/Input';
import { Link, PlayArrow, NotInterested, Explore, Input } from 'material-ui-icons';
import IconButton from 'material-ui/IconButton';
// import Home from '../components/Home';
import Table4Videos from '../components/Table4Videos';

const styles = theme => ({
  root: {
    padding: theme.spacing.unit * 2,
    flexGrow: 1,
    marginTop: theme.spacing.unit * 8,
    position: 'fixed',
    top: 0,
    bottom: 0,
    overflow: 'auto',
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  searchBar: {
    paddingTop: theme.spacing.unit * 1,
    paddingBottom: theme.spacing.unit * 1,
    paddingLeft: theme.spacing.unit * 2,
    paddingRight: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  button: {
    width: '100%',
  },
});

type Props = {
  classes: {
    root: string,
    paper: string
  }
};

type State = {
  dsMagnets: object,
  layoutStyle4Search: object,
  typeOfUrl: number,
  searchLink: string,
  isLoading: boolean,
  openSnackbar: boolean,
  transitionSnackbar: func,
  msgSnackbar: string
};

function TransitionUp(props) {
  return <Slide direction="up" {...props} />;
}

export default withStyles(styles)(class HomePage extends Component<Props, State> {
  props: Props;
  state: State;

  constructor(props, context) {
    super(props, context);
    this.state = {
      dsMagnets: [],
      layoutStyle4Search: {
        direction: 'row',
        justify: 'center',
        alignItems: 'center',
      },
      typeOfUrl: 0,
      searchLink: '',
      isLoading: false,
      openSnackbar: false,
      transitionSnackbar: null,
      msgSnackbar: '',
    };
  }

  componentWillMount() {
    ipc.on('spy-magnet-res', this.callbackDatasetMagnets);
    ipc.on('explorer-loaded', this.callbackExplorerLoaded);
    ipc.on('explorer-hide', this.callbackExplorerhide);
  }

  componentWillUnmount() {
    ipc.removeListener('spy-magnet-res', this.callbackDatasetMagnets);
    ipc.removeListener('explorer-loaded', this.callbackExplorerLoaded);
    ipc.removeListener('explorer-hide', this.callbackExplorerhide);
  }

  callbackDatasetMagnets = (eventEmitter, dsMagnets) => {
    // console.log('--dsMagnets---');
    // console.log(dsMagnets);
    if (dsMagnets.length === 0) {
      this.setState({ dsMagnets, msgSnackbar: '没找到连接' });
    } else {
      const sum = dsMagnets.length;
      this.setState({ dsMagnets, msgSnackbar: `找到${sum}个连接` });
    }
    this.setState({ openSnackbar: true, transitionSnackbar: TransitionUp, isLoading: false });
  };

  callbackExplorerLoaded = (eventEmitter) => {
    this.setState({ isLoading: false, typeOfUrl: 3 });
  };

  callbackExplorerhide = (eventEmitter) => {
    this.setState({ isLoading: false });
  };

  handleClick = () => {
    ipc.send('spy-magnet');
  }

  handleMagnetPlay = (link) => {
    ipc.send('magnet-play', link);
  }

  handleUrlExplorer = () => {
    this.setState({ isLoading: true });
    ipc.send('url-explorer', this.state.searchLink);
  }

  handleAnalyseWebPage = () => {
    this.setState({ isLoading: true });
    ipc.send('url-analyse-web-page');
  }

  handleUrlChanged = (event) => {
    const strRegx = '^((https|http|ftp|rtsp|mms)?://)'
    + '?(([0-9a-z_!~*\'().&=+$%-]+: )?[0-9a-z_!~*\'().&=+$%-]+@)?' // ftp的user@
    + '(([0-9]{1,3}.){3}[0-9]{1,3}' // IP形式的URL- 199.194.52.184
    + '|' // 允许IP和DOMAIN（域名）
    + '([0-9a-z_!~*\'()-]+.)*' // 域名- www.
    + '([0-9a-z][0-9a-z-]{0,61})?[0-9a-z].' // 二级域名
    + '[a-z]{2,6})' // first level domain- .com or .museum
    + '(:[0-9]{1,4})?' // 端口- :80
    + '((/?)|' // a slash isn't required if there is no file name
    + '(/[0-9a-z_!~*\'().;?:@&=+$,%#-]+)+/?)$';
    const regxHttp = new RegExp(strRegx);
    // console.log(regxHttp.exec(event.target.value));
    // console.log(event.target.value);
    if (event.target.value.indexOf('magnet:') === 0) {
      // console.log(event.target.value.indexOf('magnet:'));
      this.setState({ typeOfUrl: 1 });
      this.setState({ searchLink: event.target.value });
    } else if (regxHttp.test(event.target.value)) {
      this.setState({ typeOfUrl: 2 });
      this.setState({ searchLink: event.target.value });
    } else {
      this.setState({ typeOfUrl: 0 });
      this.setState({ searchLink: '' });
    }
  }

  handleCloseSanckbar = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    this.setState({ openSnackbar: false });
  };

  render() {
    const { classes } = this.props;
    // const classes = withStyles(styles)(this);
    const { dsMagnets, isLoading, msgSnackbar } = this.state;
    // console.log(dsMagnets);
    const { alignItems, direction, justify } = this.state.layoutStyle4Search;
    let btn4Link;
    if (this.state.typeOfUrl === 0) {
      btn4Link = (
        <Button size="large" disabled variant="raised" color="primary" className={classes.button}>
          <NotInterested />
        </Button>
      );
    } else if (this.state.typeOfUrl === 1) {
      btn4Link = (
        <Button size="large" disabled={isLoading} variant="raised" color="primary" className={classes.button}>
          <PlayArrow />
        </Button>
      );
    } else if (this.state.typeOfUrl === 2) { // 打开浏览器
      btn4Link = (
        <Button size="large" disabled={isLoading} variant="raised" color="primary" className={classes.button} onClick={this.handleUrlExplorer}>
          <Explore />
        </Button>
      );
    } else if (this.state.typeOfUrl === 3) { // 根据网站地址抓取连接
      btn4Link = (
        <Button size="large" disabled={isLoading} variant="raised" color="primary" className={classes.button} onClick={this.handleAnalyseWebPage}>
          <Input />
        </Button>
      );
    }
    return (
      <div className={classes.root} spacing={24}>
        <Grid container spacing={24}>
          {/* 搜索 */}
          <Grid item xs={12}>
            <Paper className={classes.searchBar}>
              <Grid
                container
                alignItems={alignItems}
                direction={direction}
                justify={justify}
                spacing={8}
              >
                <Grid item xs={10}>
                  <form className={classes.container} noValidate autoComplete="off">
                    <TextField
                      id="tfUrl"
                      // label="网站、磁力连接"
                      InputLabelProps={{
                        shrink: true,
                      }}
                      placeholder="网站、磁力连接、url"
                      // helperText="输入地址"
                      fullWidth
                      margin="normal"
                      InputProps={{
                        startAdornment: <InputAdornment position="start"><Link /></InputAdornment>,
                      }}
                      // value={input.value}
                      onChange={this.handleUrlChanged.bind(this)}
                    />
                  </form>
                </Grid>
                <Grid item xs={2}>
                  { btn4Link }
                </Grid>
              </Grid>
            </Paper>
          </Grid>
          {/* 保存列表 */}
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <Grid
                container
                alignItems={alignItems}
                direction={direction}
                justify={justify}
                spacing={8}
              >
                <Grid item xs={1}>
                  { btn4Link }
                </Grid>
              </Grid>
            </Paper>
          </Grid>

          <Grid item xs={12}>
            <Table4Videos dataset={dsMagnets} play={this.handleMagnetPlay} />
          </Grid>
          {/* <Grid item xs={12}>
            <Button variant="raised" color="primary" onClick={() => { this.handleClick(); }}>
                Hello Wor33
            </Button>
          </Grid> */}
          {/* <Grid item xs={12} sm={6}>
            <Paper className={classes.paper}>xs=12 sm=6</Paper>
          </Grid>
          <Grid item xs={12} sm={6}>
            <Paper className={classes.paper}>xs=12 sm=6</Paper>
          </Grid>
          <Grid item xs={6} sm={3}>
            <Paper className={classes.paper}>xs=6 sm=3</Paper>
          </Grid>
          <Grid item xs={6} sm={3}>
            <Paper className={classes.paper}>xs=6 sm=3</Paper>
          </Grid>
          <Grid item xs={6} sm={3}>
            <Paper className={classes.paper}>xs=6 sm=3</Paper>
          </Grid>
          <Grid item xs={6} sm={3}>
            <Paper className={classes.paper}>xs=6 sm=3</Paper>
          </Grid> */}

          <Grid
            container
            alignItems={alignItems}
            direction={direction}
            justify={justify}
            spacing={8}
          >
            <Grid item xs={12}>
              { btn4Link }
            </Grid>
          </Grid>

          <Typography variant="headline" component="h3">
            This is a sheet of paper.
          </Typography>
          <Typography component="p">
            Paper can be used to build surface or other elements for your application.
          </Typography>
        </Grid>

        <Snackbar
          open={this.state.openSnackbar}
          onClose={this.handleCloseSanckbar}
          autoHideDuration={2000}
          transition={this.state.transitionSnackbar}
          SnackbarContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={<span id="message-id">{msgSnackbar}</span>}
        />
      </div>
    );
  }
});
