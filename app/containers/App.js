// @flow
import * as React from 'react';
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';
import MenuAppBar from '../components/MenuAppBar';

const theme = createMuiTheme({
  palette: {
    // primary: {
    //   // light: will be calculated from palette.primary.main,
    //   main: '#ff4400',
    //   // dark: will be calculated from palette.primary.main,
    //   // contrastText: will be calculated to contast with palette.primary.main
    // },
    // secondary: {
    //   light: '#0066ff',
    //   main: '#0044ff',
    //   // dark: will be calculated from palette.secondary.main,
    //   contrastText: '#ffcc00',
    // },
    // // error: will us the default color
  },
});

type Props = {
  children: React.Node
};

export default class App extends React.Component<Props> {
  props: Props;

  render() {
    const styles = {
      root: {
        // height: '100%', overflow: 'auto'
      }
    };
    return (
      <MuiThemeProvider theme={theme}>
        <div style={styles.root}>
          <MenuAppBar />
          {this.props.children}
        </div>
      </MuiThemeProvider>
    );
  }
}
