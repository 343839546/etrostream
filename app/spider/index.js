// @flow
import https from 'https';
import fs from 'fs';
import cheerio from 'cheerio';
import request from 'request';
import socks5 from 'socks5-https-client';
import Agent from 'socks5-https-client/lib/Agent';

function startGrabBySocks5(options, callback) {
  // console.log(options);
  socks5.get(options, res => { analyseRes(res, callback); }).on('error', (err) => { console.log(err); });
}

function analyseRes(res, callback) {
  let html = ''; // 用来存储请求网页的整个html内容
  res.setEncoding('utf-8'); // 防止中文乱码

  res.on('data', (chunk) => { html += chunk; });
  res.on('end', () => {
    // console.log(html);
    const list = [];
    const re = /magnet:.*?(?=['|"])/gm;
    let foundRes = re.exec(html);
    while (foundRes != null) {
      // exec使arr返回匹配的第一个，while循环一次将使re在g作用寻找下一个匹配。
      // console.log(decodeURI(/dn=(.*?)&tr=udp/.exec(foundRes[0])[1]));
      const info = {};
      [info.magnet] = foundRes;
      info.title = decodeURI(/dn=(.*?)&tr=udp/.exec(foundRes[0])[1]);
      list.push(info);
      foundRes = re.exec(html);
    }

    // 海盗湾 html结构会变,直接采用正则
    // const $ = cheerio.load(html); // 采用cheerio模块解析html
    // const list = [];
    // $('td.vertTh').each((i, element) => {
    //   // console.log($(element).next().children('a').text());
    //   // console.log($(element)
    //   //   .next()
    //   //   .next()
    //   //   .next()
    //   //   .children('nobr')
    //   //   .children('a')
    //   //   .eq(0)
    //   //   .attr('href'));
    //   // console.log('----------');
    //   const info = {};
    //   info.title = $(element).next().children('a').text();
    //   info.magnet = $(element)
    //     .next()
    //     .next()
    //     .next()
    //     .children('nobr')
    //     .children('a')
    //     .eq(0)
    //     .attr('href');
    //   list.push(info);
    // });
    callback(list);
  });
}

function escape2Html(str) {
  const arrEntities = {
    lt: '<',
    gt: '>',
    nbsp: ' ',
    amp: '&',
    quot: '"'
  };
  return str.replace(/&(lt|gt|nbsp|amp|quot);/ig, (all, t) => arrEntities[t]);
}

function html2Escape(sHtml) {
  return sHtml.replace(/[<>&"]/g, (c) => ({
    '<': '&lt;',
    '>': '&gt;',
    '&': '&amp;',
    '"': '&quot;'
  }[c]));
}

export default class Spider {
  static spy(callback) {
    // const hostname = 'https://thepiratebay.org/top/48h200';
    const hostname = 'thepiratebay.org';
    const path = '/top/48h200';
    const proxyOptions = {
      hostname,
      path,
      socksHost: '127.0.0.1',
      socksPort: 2333,
      rejectUnauthorized: false
    };
    startGrabBySocks5(proxyOptions, callback);
  }

  static regxFoundMagnet(html, callback) {
    const list = [];
    const re = /magnet:.*?(?=['|"])/gm;
    let foundRes = re.exec(html);
    while (foundRes != null) {
      // exec使arr返回匹配的第一个，while循环一次将使re在g作用寻找下一个匹配。
      // console.log(decodeURI(/dn=(.*?)&tr=udp/.exec(foundRes[0])[1]));
      const info = {};
      [info.magnet] = foundRes;
      const str = escape2Html(foundRes[0]);
      const searchTitle = /dn=(.*?)&tr=udp/.exec(str);
      // console.log(searchTitle);
      if (searchTitle === null) {
        info.title = '';
        info.titleAlt = str;
      } else {
        info.title = decodeURI(searchTitle[1]);
        info.titleAlt = decodeURI(searchTitle[1]);
      }
      list.push(info);
      foundRes = re.exec(html);
    }
    callback(list);
  }
}

